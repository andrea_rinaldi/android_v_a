package vale512.github.io.dndapp;

import android.arch.persistence.room.Room;
import android.content.Context;

public class DatabaseFactory {

    private static AppDatabase ISTANCE = null;

    public static AppDatabase getOrCreate(Context context) {
        if(ISTANCE == null)
            ISTANCE = Room.databaseBuilder(context, AppDatabase.class, "dnd-app-database").allowMainThreadQueries().build();
        return ISTANCE;
    }

}
