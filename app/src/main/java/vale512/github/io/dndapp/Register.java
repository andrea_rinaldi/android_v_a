package vale512.github.io.dndapp;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import vale512.github.io.dndapp.models.User;

public class Register extends AppCompatActivity {

    private Button registrationSubmit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registrationSubmit = findViewById(R.id.registrationButton);

        //Instantiating the Entity of the DB
        final AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "dnd-app-database").allowMainThreadQueries().build();

        registrationSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                check();
                toastUser(db);
            }
        });
    }

    private void toastUser(AppDatabase db){
        User valeriuccio = db.userDao().findByNickname("Andre");
        Log.d("DB_READING","Leggendo Con Valeriuccio");
        try {
            Toast.makeText(getApplicationContext(), valeriuccio.getPassword() + " " + valeriuccio.getNickname(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.e("ERR",e.getMessage());
            Toast.makeText(getApplicationContext(), "Nessun utente trovato", Toast.LENGTH_LONG).show();
        }

    }

    private void activityChangeLogin() {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.register_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        activityChangeLogin();
        return super.onOptionsItemSelected(item);
    }


    private boolean check() {
        InputValidator inputValidator = new InputValidator();

        //Validating Email Input Field
        EditText emailText = findViewById(R.id.registerEditEmail);
        String email = emailText.getText().toString();
        if (!inputValidator.validateEmail(email)) {
            Toast.makeText(getApplicationContext(), "Inserire un E-mail valida", Toast.LENGTH_LONG).show();
            return false;
        }

        //Validating Nickname Input
        EditText nicknameText = findViewById(R.id.registerEditNickname);
        String nickname = nicknameText.getText().toString();
        if (!inputValidator.validateRequired(nickname)) {
            Toast.makeText(getApplicationContext(), "Inserire un Nickname", Toast.LENGTH_LONG).show();
            return false;
        }

        //Validating Password Input and Checking if they're equal
        EditText passwordText1 = findViewById(R.id.registerEditPsw);
        String password1 = passwordText1.getText().toString();
        EditText passwordText2 = findViewById(R.id.registerEditPsw2);
        String password2 = passwordText2.getText().toString();
        if (!inputValidator.validatePasswords(password1, password2)) {
            Toast.makeText(getApplicationContext(), "Inserire due password uguali", Toast.LENGTH_LONG).show();
            return false;
        }

        //TODO sistemare
        DatabaseDAO dao = new DatabaseDAO(this);
        dao.insert(nickname, email, password1);
            return true;

    }

}
