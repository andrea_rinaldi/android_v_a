package vale512.github.io.dndapp;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import vale512.github.io.dndapp.models.User;


@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}

