package vale512.github.io.dndapp;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import vale512.github.io.dndapp.models.User;


public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button testButton = findViewById(R.id.testButton);
        testButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                check();
            }
        });
    }

    //TODO test
    private void insertSomeRandomData(){
        AppDatabase db = DatabaseFactory.getOrCreate(getApplicationContext());
        User a = new User("andreuccio@andr.it", "Andre", "adreuccino");
        db.userDao().insertAll(a);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        activityChangeRegister();
        return super.onOptionsItemSelected(item);
    }

    //Change Activity to Login
    private void activityChangeRegister() {
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }


    private boolean check() {
        EditText emailText = findViewById(R.id.loginEditEmail);
        String email = emailText.getText().toString();

        EditText passwordText = findViewById(R.id.loginEditPsw);
        String password = passwordText.getText().toString();

        AppDatabase db = DatabaseFactory.getOrCreate(getApplicationContext());
        User user = db.userDao().findByEmail(email);
        int userId = user.getUid();

        loginChangeDashboard(userId);

        return false;

    }


    private void loginChangeDashboard(int userId) {
        Intent intent = new Intent(this, Dashboard.class);
        intent.putExtra("userId",userId);
        startActivity(intent);
    }

}
