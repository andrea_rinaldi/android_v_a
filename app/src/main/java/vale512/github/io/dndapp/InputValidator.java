package vale512.github.io.dndapp;


import android.util.Patterns;

public class InputValidator{

    public boolean validateEmail(String email){
        if(email.isEmpty())return false;
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean validateRequired(String input){
       return !input.isEmpty();
    }


    public boolean validatePasswords(String psw1, String psw2){
        if(!psw1.isEmpty() && !psw2.isEmpty()){
            return psw1.equals(psw2);
        }
        return false;
    }


}
